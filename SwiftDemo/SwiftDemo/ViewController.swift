//
//  ViewController.swift
//  SwiftDemo
//
//  Created by HLBB ISD CoE #1 on 31/07/2017.
//  Copyright © 2017 HongLeongBank. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        var myVariable = 42
        myVariable = 50
        let myConstant = 42
        print("Hello GRT Welcome to Swift =",myVariable,myConstant)
        
        let implicitinteger = 50
        let implicitdouble  = 50.0
        let explicitdouble: Double  = 50
        
        print(implicitinteger,implicitdouble,explicitdouble)
        
        let newConstant: Float = 1
        
        print(newConstant)
        
        let label = "This is GRT ="
        
        let width = 500
        let result = label + String(width)
        
        print(result)
        
        //string
        
        let apples = 3
        let oranges = 5
        let appleSummary = "I have \(apples) apples"
        let orangeSummary = "I have \(apples + oranges) fruits"
        
        print(apples,oranges,appleSummary,orangeSummary)
        
        // Array
        
        var shoppingList = ["catfish", "water", "tulips", "blue paint"]
        
        print(shoppingList)
        shoppingList[1] = "bottle of water"
        print(shoppingList)
        
        var occupations = [ "Malcolm": "Captain","Kaylee": "Mechanic",]
        print(occupations)
        occupations["Jayne"] = "Public Relations"
        print(occupations)

   
        
        let emptyArray = [String]()
        let emptyDict = [String:Float]()
        
        print(emptyDict)
        
    // For loop
        
        let individualScores = [75,43,105,80,22]
        var teamscore = 0
        
        for score in individualScores {
            if score > 50 {
                teamscore += 3
            }
            else
            {
                teamscore += 1
            }
        }
        print(teamscore)
        
//        var optionalString: String? = "Hello"
//        print(optionalString == nil)
        
        
        // if else
        
        var optionalName: String? = "Raviteja"
        var greeting = "Hello!"
        if let name = optionalName {
            greeting = "Hello, \(name)"
        }
        else
        {
            greeting = "Hello,Gadige"
        }
        print(greeting)
        
        let name1: String? = nil
        let name2: String  = "Raviteja"
        let resultvalue =  "Hi \(name1 ?? name2)"
        print(resultvalue)
        
      // Switch
        let vegetable = "red pepper"
        switch vegetable {
        case "celery":
            print("Add some raisins and make ants on a log.")
        case "cucumber", "watercress":
            print("That would make a good tea sandwich.")
        case let x where x.hasSuffix("pepper"):
            print("Is it a spicy \(x)?")
        default:
            print("Everything tastes good in soup.")
        }
        
        //
        
        let interestingNumbers = [
            "Prime": [2, 3, 5, 7, 11, 13],
            "Fibonacci": [1, 1, 2, 3, 5, 8],
            "Square": [1, 4, 9, 16, 25],
            ]
        var largest = 0
        for (kind, numbers) in interestingNumbers {
            for number in numbers {
                if number > largest {
                    largest = number
                }
            }
        }
        print(largest)
    
    }
    
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

